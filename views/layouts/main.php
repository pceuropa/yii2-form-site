<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\widgets\Alert;
AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <meta property="og:url"           content="<?php echo Yii::$app->request->absoluteUrl; ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:image"         content="https://form.pceuropa.net/images/logo.png" />
</head>

<body>
<?php $this->beginBody() ?>

		<?= $this->render('_navbar');?>

		<div class="wrap container">
		    <?= Breadcrumbs::widget([ 
		    	'homeLink' => [
    				'label' => '<span class="glyphicon glyphicon-home" aria-hidden="true"></span>',
		  			'encode' => false,
		  			'url' => Url::home(),
					],
		    	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], ]) ?>
            <?= Alert::widget() ?>

		    <?= $content ?>
              <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code -->
  <div class="pull-right fb-share-button"
    data-href="<?php echo Yii::$app->request->absoluteUrl; ?>"
    data-layout="button_count">
  </div>
		</div>
		
<?= $this->render('_footer.php'); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
