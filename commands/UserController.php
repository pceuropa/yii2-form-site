<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\User;

/**
 * This command echoes the first argument that you have entered.
 * This command is provided as an example for you to learn how to create console commands.
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello user')
    {
        echo $message . "\n";
        return ExitCode::OK;
    }

    /**
     * Create superuser
     * @param string $email email address of user
     * @return Null
     */
    public function actionCreate(string $email='kp@incepton.com'){
          $user = new User();
          $user->email = $email;;
          $user->username = $email;
          $user->setPassword('password');
          $user->generateAuthKey();

          if ($user->save()) {
            echo 'Created user';
            echo "Email kp@incepton.com";
            echo "Password: passowrd";
            return ExitCode::OK;
          } else {
            print_r($user->getErrors());
          }
           
    }

    /**
     * Create rules
     * @return Null
     */
    public function actionCreateRoles()
    {
        $auth = \Yii::$app->authManager;
        $user = $auth->getRole('user');
        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $user);
    }

    /**
     * Create rules
     * @return Null
     */
    public function actionAssignRoles()
    {
        $auth = \Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $auth->assign($admin, 1);
    }
    
    /**
     * Delete all rules
     * @return Null
     */
    public function actionDeleteRoles()
    {
        $auth = \Yii::$app->authManager;
        $auth->removeAll();
    }
}
